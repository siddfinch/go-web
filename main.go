package main

import (
        "flag"
        "log"
        "net/http"
        "time"
)

func RequestLogger(targetMux http.Handler) http.Handler {
        return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
                start := time.Now()
                targetMux.ServeHTTP(w, r)
                log.Printf(
                        "%s\t\t%s\t\t%s\t\t%v",
                        r.Method,
                        r.RequestURI,
                        r.RemoteAddr,
                        time.Since(start),
                )
        })
}

func main() {
        port := flag.String("port", "9999", "port to serve")
        directory := flag.String("directory", ".", "directory to serve")
        flag.Parse()
        log.Printf("Server %s on port: %s\n", *directory, *port)

        mux := http.NewServeMux()
        mux.Handle("/", http.FileServer(http.Dir(*directory)))
        if err := http.ListenAndServe(":"+*port, RequestLogger(mux)); err != nil {
                log.Fatal(err)
        }
}
