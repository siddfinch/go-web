default: all

ARCH = linux darwin windows

all: $(ARCH:%=go-web.%)

go-web.%: main.go
        @env GOOS=$* go build -o $@

clean:
        @rm -f $(ARCH:%=go-web.%)
