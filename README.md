# go-web

## Why Go?

Good question: Mainly because it allowed me to make a quick web server that should run on Windows,
Mac, and Linux. ( Among others, Plan 9 if one wanted, but W,M and L seem to be the big three. )

Wait, I should have on the server in Plan 9, now that I think of it. Or, hmm, perhaps Inferno.

Any way, This is a simple web server that fits perfect to service a directory of static files. It uses the go standard library and nothing else. There is limited logging, but enough to know if something is connecting to the server.

```
package main

import (
        "flag"
        "log"
        "net/http"
        "time"
)
```

See, nothing special.

```
func RequestLogger(targetMux http.Handler) http.Handler {
        return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
                start := time.Now()
                targetMux.ServeHTTP(w, r)
                log.Printf(
                        "%s\t\t%s\t\t%s\t\t%v",
                        r.Method,
                        r.RequestURI,
                        r.RemoteAddr,
                        time.Since(start),
                )
        })
}
```
Looks like a lot, but really this is just running a http handler functions that create a mux to server http and log some basic info to stdout. This should be enough to diagnose any problems. If you need more information, go-web is probably not what you need.
```
func main() {
        port := flag.String("port", "9999", "port to serve")
        directory := flag.String("directory", ".", "directory to serve")
        flag.Parse()
        log.Printf("Server %s on port: %s\n", *directory, *port)

        mux := http.NewServeMux()
        mux.Handle("/", http.FileServer(http.Dir(*directory)))
        if err := http.ListenAndServe(":"+*port, RequestLogger(mux)); err != nil {
                log.Fatal(err)
        }
}
```

And this is where the magic happens:

We have a two flags, to specific the port the web server runs on and the directory to server. Defaults to servering the current directory you are in. Not the safest feature.

The we create a new server mux, and setup the fileServer handler for the directory. We feed the mux into the http.ListenandServer handler, telling it to use the request logger function we created.

Done.

```
=; ./go-web.linux -h
Usage of ./go/go-web/go-web.linux:
  -directory string
        directory to serve (default ".")
  -port string
        port to serve (default "9999")
```